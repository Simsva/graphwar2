#!/usr/bin/env python3

import pygame as pg
import time
import os
from dataclasses import dataclass

@dataclass
class COLOR:
    BLACK = pg.Color(0, 0, 0)
    WHITE = pg.Color(255, 255, 255)

# TODO: Base class for UI elements
class InputBox:
    def __init__(self, font, x, y, w, h, callback, text=""):
        self.rect = pg.Rect(x, y, w, h)
        self.pref_w = w
        self.color = COLOR.WHITE
        self.text = text
        self.font = font
        self.callback = callback
        self.surface = font.render(text, True, self.color)
        self.active = False

    def handle_event(self, e):
        if e.type == pg.MOUSEBUTTONDOWN:
            if self.rect.collidepoint(e.pos):
                self.active = not self.active
            else:
                self.active = False

        elif e.type == pg.KEYDOWN and self.active:
            if e.key == pg.K_RETURN:
                self.ret()
            elif e.key == pg.K_BACKSPACE:
                self.text = self.text[:-1]
            else:
                self.text += e.unicode

            self.surface = self.font.render(self.text, True, self.color)

    def update(self):
        self.rect.w = max(self.pref_w, self.surface.get_width()+10)

    def draw(self, screen):
        screen.blit(self.surface, (self.rect.x+5, self.rect.y+5))
        pg.draw.rect(screen, self.color, self.rect, 2)

    def ret(self):
        self.callback(self)

class Game:
    def __init__(self, root):
        self.ROOT_PATH = root
        self.running = True

        self.iboxes = []

        pg.init()

        pg.display.set_caption("Graphwar 2")
        self.win = pg.display.set_mode((1280, 720))
        self.clock = pg.time.Clock()

    def handle_events(self):
        for e in pg.event.get():
            if e.type == pg.QUIT:
                self.running = False

            for box in self.iboxes:
                box.handle_event(e)

    def norm_path(self, path):
        return os.path.join(self.ROOT_PATH, path)

    def render_text(self, font, color, text, x=0, y=0):
        surface = font.render(text, True, color)
        rect = surface.get_rect()
        rect[0] += x; rect[2] += x
        rect[1] += y; rect[3] += y
        self.win.blit(surface, rect)

    # TODO: hooks
    def loop(self, bruh):
        while self.running:
            self.handle_events()

            for box in self.iboxes:
                box.update()

            self.win.fill(COLOR.BLACK)

            for box in self.iboxes:
                box.draw(self.win)

            # FIXME: move to hook
            self.render_text(bruh, COLOR.WHITE, f"FPS:{self.clock.get_fps():.0f}")

            pg.display.flip()
            self.clock.tick()


def main():
    ROOT_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))

    game = Game(ROOT_PATH)

    font = pg.font.Font(game.norm_path("assets/fonts/LiberationMono-Regular.ttf"), 20)

    game.iboxes.append(InputBox(font, 100, 100, 100, 30, lambda x: print(x.text)))

    game.loop(font)

if __name__ == "__main__":
    main()
